package com.app.pinoymobilekitchen.modules.Library.fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.app.pinoymobilekitchen.MainActivity;
import com.app.pinoymobilekitchen.R;
import com.app.pinoymobilekitchen.modules.DishDetails.DishDetailsActivity;
import com.app.pinoymobilekitchen.DishModel;
import com.app.pinoymobilekitchen.modules.Library.UploadActivity;
import com.app.pinoymobilekitchen.modules.Library.adapters.LibraryAdapter;
import com.app.pinoymobilekitchen.modules.Profile.ProfileFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class LibraryFragment extends Fragment implements LibraryAdapter.ItemClickListener, LibraryAdapter.ArchiveClickListener {

    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    View v;

    private FloatingActionButton fabUpload, fabArchive;
    private LinearLayout llSignInContainer;
    private Button btnSignIn;
    private RecyclerView recyclerView;
    private LibraryAdapter adapter;
    private List<DishModel> libraryModels, archivedModels;
    private ProgressBar pbLoading;
    private int libraryCount, archiveCount;

    private FirebaseDatabase database;

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;


    public LibraryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        //SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            //searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    if(adapter!=null){
                        String userInput = newText.toLowerCase();
                        List<DishModel> newModels = new ArrayList<>();
                        for(DishModel model : libraryModels){
                            if(model.getRecipeName().toLowerCase().contains(userInput)){
                                newModels.add(model);
                            }
                        }
                        adapter.setModelList(newModels);
                        return true;
                    }

                    return false;
                }
                @Override
                public boolean onQueryTextSubmit(String query) {

                    return false;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_library, container, false);

        fabUpload = v.findViewById(R.id.fabLibrary_Upload);
        fabArchive = v.findViewById(R.id.fabLibrary_Archive);
        llSignInContainer = v.findViewById(R.id.llLibrary_SignInContainer);
        btnSignIn = v.findViewById(R.id.btnLibrary_SignIn);

        recyclerView = v.findViewById(R.id.rvLibrary);
        pbLoading = v.findViewById(R.id.pbLibrary_Loading);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        //init firebase
        database = FirebaseDatabase.getInstance();

        updateUI();

        fabUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getFragmentManager().beginTransaction().replace(R.id.content_frame, new UploadFragment()).addToBackStack(null).commit();
                startActivityForResult(new Intent(getActivity(), UploadActivity.class), 1);
            }
        });

        fabArchive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame, ArchiveFragment.newInstance(archivedModels)).addToBackStack(null).commit();
            }
        });

        return v;
    }

    @SuppressLint("RestrictedApi")
    private void updateUI() {
        if (currentUser != null) {
            pbLoading.setVisibility(View.VISIBLE);
            fabUpload.setVisibility(View.VISIBLE);
            fabArchive.setVisibility(View.VISIBLE);
            DatabaseReference libraryRef = database.getReference("Library").child(currentUser.getUid());
            libraryRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        getLibrary();
                    } else pbLoading.setVisibility(View.GONE);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } else {
            llSignInContainer.setVisibility(View.VISIBLE);
            btnSignIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getFragmentManager().beginTransaction().replace(R.id.content_frame, new ProfileFragment()).addToBackStack(null).commit();
                    ((MainActivity) getActivity()).navView.setSelectedItemId(R.id.nav_profile);
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            getLibrary();
        }
        //super.onActivityResult(requestCode, resultCode, data);
    }

    private void getLibrary() {
        pbLoading.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

        libraryCount = 0;
        archiveCount = 0;
        libraryModels = new ArrayList<>();
        archivedModels = new ArrayList<>();
        DatabaseReference libraryRef = database.getReference("Library").child(currentUser.getUid());

        libraryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        getDishDetails(child.child("Category").getValue().toString(), child.child("Video Key").getValue().toString());
                        //Log.e("Library", "after loop: model size="+libraryModels.size()+", expected count="+libraryCount);
                    }
                }
                else {
                    pbLoading.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getDishDetails(final String category, String videoKey) {
        DatabaseReference categoryRef = database.getReference("Category").child(category).child(videoKey);

        categoryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DishModel model = (new DishModel(
                        category,
                        dataSnapshot.getKey(),
                        dataSnapshot.child("Dislike").getValue().toString(),
                        dataSnapshot.child("Like").getValue().toString(),
                        dataSnapshot.child("Rate").getValue().toString(),
                        dataSnapshot.child("Description").getValue().toString(),
                        dataSnapshot.child("Ingredients").getValue().toString(),
                        dataSnapshot.child("Procedures").getValue().toString(),
                        dataSnapshot.child("Recipe Name").getValue().toString(),
                        dataSnapshot.child("Report Count").getValue().toString(),
                        dataSnapshot.child("Uploader ID").getValue().toString(),
                        dataSnapshot.child("Uploader Name").getValue().toString(),
                        dataSnapshot.child("Uploader ProfPic").exists() ? dataSnapshot.child("Uploader ProfPic").getValue().toString() : null,
                        dataSnapshot.child("Video Thumbnail").getValue().toString(),
                        dataSnapshot.child("Video URL").getValue().toString(),
                        dataSnapshot.child("View Count").getValue().toString(),
                        (boolean) dataSnapshot.child("Archived").getValue(),
                        (boolean) dataSnapshot.child("Banned").getValue()
                ));
                if (!model.getBanned()) {
                    if (model.getArchived()){
                        archivedModels.add(model);
                        archiveCount+=1;
                    }
                    else{
                        libraryModels.add(model);
                        libraryCount+=1;
                    }
                }
                //Log.e("Library", "getDishDetails: model size="+libraryModels.size()+", expected count="+libraryCount);

                if(libraryModels.size()==libraryCount && archivedModels.size()==archiveCount) {
                    loadIntoAdapter();
                    pbLoading.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void loadIntoAdapter(){
        //Log.e("Library", "before adapter: model size="+libraryModels.size()+", expected count="+libraryCount);
        adapter = new LibraryAdapter(getActivity(), libraryModels, LibraryFragment.this, LibraryFragment.this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClicked(String key, String category) {
        Intent intent = new Intent(getActivity(), DishDetailsActivity.class);
        intent.putExtra("category", category);
        intent.putExtra("key", key);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onArchiveClicked(final int position) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setIcon(R.drawable.ic_archive);
        alertDialog.setTitle("Move to Archive?");
        alertDialog.setMessage("Moving this item to archive will prevent other users from seeing this item. Are you sure you want to move this to archive?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DatabaseReference categoryRef = database.getReference("Category").child(libraryModels.get(position).getCategory()).child(libraryModels.get(position).getKey());
                categoryRef.child("Archived").setValue(true);
                libraryModels.get(position).setArchived(true);
                archivedModels.add(libraryModels.get(position));
                libraryModels.remove(position);
                adapter.setModelList(libraryModels);
                dialog.dismiss();
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle("Library");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (searchView != null) {
            searchView.setQuery("", false);
            searchView.clearFocus();
            searchView.onActionViewCollapsed();
        }
    }
}
