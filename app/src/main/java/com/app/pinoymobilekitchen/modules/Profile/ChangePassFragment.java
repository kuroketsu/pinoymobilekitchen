package com.app.pinoymobilekitchen.modules.Profile;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.app.pinoymobilekitchen.MainActivity;
import com.app.pinoymobilekitchen.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePassFragment extends Fragment {

    View v;

    Button btnChangePass;
    TextInputEditText etNew, etConfirm;


    public ChangePassFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_change_pass, container, false);
        etNew = v.findViewById(R.id.etChangePass_New);
        etConfirm = v.findViewById(R.id.etChangePass_Confirm);
        btnChangePass = v.findViewById(R.id.btnChanePass_Change);

        btnChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etNew.getText().toString().isEmpty() || etConfirm.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), "Empty Fields Not Allowed", Toast.LENGTH_SHORT).show();
                }
                else {
                    if(etNew.getText().toString().equals(etConfirm.getText().toString())) {
                        changePass();
                    }
                    else {
                        Toast.makeText(getActivity(), "Password Did Not Match", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        return v;
    }

    private void changePass() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        user.updatePassword(etConfirm.getText().toString())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getActivity(), "Password Successfully Changed", Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();
                        }
                        else {
                            Toast.makeText(getActivity(), "Password Change Failed. Please Try Again", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle("Change Password");
    }

}
