package com.app.pinoymobilekitchen.modules.Home;


import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.pinoymobilekitchen.MainActivity;
import com.app.pinoymobilekitchen.R;
import com.app.pinoymobilekitchen.modules.DishList.DishListFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    View v;

    CardView beef, pork, chicken, fish, vegetables, dessert;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_home, container, false);

        beef = v.findViewById(R.id.cvHome_Beef);
        pork = v.findViewById(R.id.cvHome_Pork);
        chicken = v.findViewById(R.id.cvHome_Chicken);
        fish = v.findViewById(R.id.cvHome_Fish);
        vegetables = v.findViewById(R.id.cvHome_Vegetable);
        dessert = v.findViewById(R.id.cvHome_Dessert);

        beef.setOnClickListener(this);
        pork.setOnClickListener(this);
        chicken .setOnClickListener(this);
        fish.setOnClickListener(this);
        vegetables.setOnClickListener(this);
        dessert.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        String category = "";
        switch (v.getId()){
            case R.id.cvHome_Beef:
                category = "Beef";
                break;
            case R.id.cvHome_Pork:
                category = "Pork";
                break;
            case R.id.cvHome_Chicken:
                category = "Chicken";
                break;
            case R.id.cvHome_Fish:
                category = "Seafood";
                break;
            case R.id.cvHome_Vegetable:
                category = "Vegetables";
                break;
            case R.id.cvHome_Dessert:
                category = "Dessert";
                break;
        }
        getFragmentManager().beginTransaction().replace(R.id.content_frame, DishListFragment.newInstance(category)).addToBackStack(null).commit();
    }

    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle(getActivity().getResources().getString(R.string.app_name));
    }
}
