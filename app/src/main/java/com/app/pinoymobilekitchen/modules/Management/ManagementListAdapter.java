package com.app.pinoymobilekitchen.modules.Management;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pinoymobilekitchen.DishModel;
import com.app.pinoymobilekitchen.R;
import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ManagementListAdapter extends RecyclerView.Adapter<ManagementListAdapter.MyViewHolder> {

    private Context context;
    private List<DishModel> models;
    private ItemClickListener itemClickListener;
    private OptionsClickListener optionsClickListener;
    private String status;

    public ManagementListAdapter(Context context, List<DishModel> models, String status, ItemClickListener itemClickListener, OptionsClickListener optionsClickListener) {
        this.context = context;
        this.models = models;
        this.status = status;
        this.itemClickListener = itemClickListener;
        this.optionsClickListener = optionsClickListener;
    }

    public void setModelList(List<DishModel> models) {
        this.models = models;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ManagementListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dish, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ManagementListAdapter.MyViewHolder holder, final int position) {
        holder.tvRecipeName.setText(models.get(position).getRecipeName());
        holder.tvUploaderName.setText(models.get(position).getUploaderName());
        holder.tvViewCount.setText(models.get(position).getViewCount());
        Glide.with(context).load(models.get(position).getVideoThumbnail())
                .into(holder.ivThumbnail);
        Glide.with(context).load(models.get(position).getUploaderProfPic())
                .placeholder(R.drawable.ic_person)
                .into(holder.ivProfPic);
        holder.rlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClicked(models.get(position).getKey(), models.get(position).getCategory());
            }
        });
        holder.tvOptionsMenu.setVisibility(View.VISIBLE);
        holder.tvOptionsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, holder.tvOptionsMenu);
                popup.inflate(R.menu.management_item_menu);
                switch (status){
                    case "Active":
                        popup.getMenu().getItem(0).setVisible(true); // ignore
                        popup.getMenu().getItem(1).setVisible(true); // ban
                        break;
                    case "Ignored":
                        popup.getMenu().getItem(1).setVisible(true); // ban
                        popup.getMenu().getItem(2).setVisible(true); // restore
                        break;
                    case "Banned":
                        popup.getMenu().getItem(2).setVisible(true); // restore
                        break;
                }
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_management_item_ignore:
                                optionsClickListener.onOptionsClicked(models.get(position).getKey(), models.get(position).getCategory(), "Ignored", models.get(position).getManagementKey());
                                return true;
                            case R.id.menu_management_item_ban:
                                optionsClickListener.onOptionsClicked(models.get(position).getKey(), models.get(position).getCategory(), "Banned", models.get(position).getManagementKey());
                                return true;
                            case R.id.menu_management_item_restore:
                                optionsClickListener.onOptionsClicked(models.get(position).getKey(), models.get(position).getCategory(), "Active", models.get(position).getManagementKey());
                                return true;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });
    }

    public interface ItemClickListener{
        void onItemClicked(String key, String category);
    }

    public interface OptionsClickListener{
        void onOptionsClicked(String key, String category, String action, String managementKey);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvRecipeName, tvUploaderName, tvViewCount;
        ImageView ivThumbnail;
        CircleImageView ivProfPic;
        LinearLayout rlContainer;

        TextView tvOptionsMenu;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvRecipeName = itemView.findViewById(R.id.tvDishItem_RecipeName);
            tvUploaderName = itemView.findViewById(R.id.tvDishItem_UploaderName);
            tvViewCount = itemView.findViewById(R.id.tvDishItem_ViewCount);
            ivThumbnail = itemView.findViewById(R.id.ivDishItem_Image);
            ivProfPic = itemView.findViewById(R.id.ivDishItem_ProfPic);
            rlContainer = itemView.findViewById(R.id.rlDishItem_Container);

            tvOptionsMenu = itemView.findViewById(R.id.tvDishItem_Options);
        }
    }
}
