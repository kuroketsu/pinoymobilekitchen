package com.app.pinoymobilekitchen.modules.Library.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.app.pinoymobilekitchen.DishModel;
import com.app.pinoymobilekitchen.MainActivity;
import com.app.pinoymobilekitchen.R;
import com.app.pinoymobilekitchen.modules.DishDetails.DishDetailsActivity;
import com.app.pinoymobilekitchen.modules.Library.adapters.ArchiveAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArchiveFragment extends Fragment implements ArchiveAdapter.ItemClickListener, ArchiveAdapter.RestoreClickListener {

    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    private View v;
    private RecyclerView recyclerView;
    private ArchiveAdapter adapter;

    private FirebaseDatabase database;

    private void setModels(List<DishModel> models) {
        this.models = models;
    }

    List<DishModel> models;

    public static ArchiveFragment newInstance(List<DishModel> models) {
        ArchiveFragment fragment = new ArchiveFragment();
        fragment.setModels(models);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        //SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            //searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    if(adapter!=null){
                        String userInput = newText.toLowerCase();
                        List<DishModel> newModels = new ArrayList<>();
                        for(DishModel model : models){
                            if(model.getRecipeName().toLowerCase().contains(userInput)){
                                newModels.add(model);
                            }
                        }
                        adapter.setModelList(newModels);
                        return true;
                    }

                    return false;
                }
                @Override
                public boolean onQueryTextSubmit(String query) {

                    return false;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_archive, container, false);

        recyclerView = v.findViewById(R.id.rvArchive);

        //init firebase
        database = FirebaseDatabase.getInstance();

        if(models != null) {
            if (models.size() != 0) {
                adapter = new ArchiveAdapter(getActivity(), models, ArchiveFragment.this, ArchiveFragment.this);
                recyclerView.setAdapter(adapter);
            }
        }

        return v;
    }

    @Override
    public void onItemClicked(String key, String category) {
        Intent intent = new Intent(getActivity(), DishDetailsActivity.class);
        intent.putExtra("category", category);
        intent.putExtra("key", key);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onRestoreClicked(final int position) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setIcon(R.drawable.ic_unarchive);
        alertDialog.setTitle("Remove from Archive?");
        alertDialog.setMessage("Removing this item from archive will allow other users to see this item again. Are you sure you want to remove this from archive?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DatabaseReference categoryRef = database.getReference("Category").child(models.get(position).getCategory()).child(models.get(position).getKey());
                models.remove(position);
                categoryRef.child("Archived").setValue(false);
                adapter.setModelList(models);
                dialog.dismiss();
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle("Archive");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (searchView != null) {
            searchView.setQuery("", false);
            searchView.clearFocus();
            searchView.onActionViewCollapsed();
        }
    }
}
