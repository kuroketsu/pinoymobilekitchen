package com.app.pinoymobilekitchen.modules.DishDetails;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.pinoymobilekitchen.R;
import com.app.pinoymobilekitchen.DishModel;
import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class DishDetailsActivity extends AppCompatActivity {

    PlayerView vvVideo;
    TextView tvRecipeName, tvUploaderName, tvViews, tvLikes, tvDislikes, tvDescription, tvIngredients, tvProcedures;
    ImageView ivLike, ivDislike;
    CircleImageView ivUploaderProfPic;
    LinearLayout llReport, llRating;
    RatingBar rbRating;
    SimpleExoPlayer exoPlayer;
    UserViewDetailModel userViewDetailModel;
    ScrollView svMainContainer;
    ProgressBar pbLoading;
    EditText etAddComment;
    Button btnAddComment, btnCancelComment;
    LinearLayout llCommentContainer, llCommentButtonContainer;
    ArrayList<CommentsModel> commentsModels = new ArrayList<>();

    private FirebaseDatabase database;
    private DatabaseReference categoryRef, actionsRef, historyRef, managementRef, commentRef;
    private FirebaseUser currentUser;
    private FirebaseAuth mAuth;

    DishModel model;
    String key;
    String category;

    boolean ratingBarInitialized = false;

    private boolean isFullscreen = false;
    private FrameLayout mFullScreenButton;
    private ImageView mFullScreenIcon;
    Dialog mFullScreenDialog;
    private long currentPlaybackPosition = 0;
    private ProgressBar pbBuffering;
    private ImageView ivPlay;
    private boolean isRepeated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dish_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        key = getIntent().getStringExtra("key");
        category = getIntent().getStringExtra("category");

        vvVideo = findViewById(R.id.vvDishDetails_Video);
        tvRecipeName = findViewById(R.id.tvDishDetails_RecipeName);
        tvUploaderName = findViewById(R.id.tvDishDetails_UploaderName);
        tvViews = findViewById(R.id.tvDishDetails_Views);
        ivUploaderProfPic = findViewById(R.id.ivDishDetails_UploaderProfPic);
        tvLikes = findViewById(R.id.tvDishDetails_Like);
        tvDislikes = findViewById(R.id.tvDishDetails_Dislike);
        ivLike = findViewById(R.id.ivDishDetails_Like);
        ivDislike = findViewById(R.id.ivDishDetails_Dislike);
        tvDescription = findViewById(R.id.tvDishDetails_Description);
        tvIngredients = findViewById(R.id.tvDishDetails_Ingredients);
        tvProcedures = findViewById(R.id.tvDishDetails_Procedures);
        llReport = findViewById(R.id.llDishDetails_Report);
        llRating = findViewById(R.id.llDishDetails_Rating);
        rbRating = findViewById(R.id.rbDishDetails_Rating);
        svMainContainer = findViewById(R.id.svDishDetails_MainContainer);
        pbLoading = findViewById(R.id.pbDishDetails_Loading);
        pbBuffering = findViewById(R.id.pbDishDetails_Buffering);
        ivPlay = findViewById(R.id.ivDishDetails_Play);
        etAddComment = findViewById(R.id.etDishDetails_AddComment);
        btnAddComment = findViewById(R.id.btnDishDetails_AddComment);
        btnCancelComment = findViewById(R.id.btnDishDetails_CancelComment);
        llCommentContainer = findViewById(R.id.llDishDetails_CommentContainer);
        llCommentButtonContainer = findViewById(R.id.llDishDetails_CommentButtonContainer);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        database = FirebaseDatabase.getInstance();

        categoryRef = database.getReference("Category").child(category).child(key);
        commentRef = database.getReference("Category").child(category).child(key).child("Comments");

        getDishDetails();

        ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(exoPlayer!=null){
                    if(isRepeated){
                        exoPlayer.seekTo(0);
                    }
                    exoPlayer.setPlayWhenReady(true);
                    ivPlay.setVisibility(View.GONE);
                    ivPlay.setImageResource(R.drawable.ic_play);

                    if(model!=null) categoryRef.child("View Count").setValue(Integer.parseInt(model.getViewCount())+1+"");
                }
            }
        });

        etAddComment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    llCommentButtonContainer.setVisibility(View.VISIBLE);
                }
                else {
                    llCommentButtonContainer.setVisibility(View.GONE);
                    InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(etAddComment.getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        });

        btnAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentUser!=null) {
                    if (!etAddComment.getText().toString().isEmpty()) {
                        DatabaseReference commentRef = database.getReference("Category").child(category).child(key).child("Comments").push();
                        commentRef.child("user_name").setValue(currentUser.getDisplayName());
                        if (currentUser.getPhotoUrl() != null) commentRef.child("user_prof_pic").setValue(currentUser.getPhotoUrl().toString());
                        commentRef.child("message").setValue(etAddComment.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                CommentsModel commentsModel = new CommentsModel(
                                        etAddComment.getText().toString(),
                                        currentUser.getDisplayName(),
                                        currentUser.getPhotoUrl() != null ? currentUser.getPhotoUrl().toString() : null
                                );

                                inflateComment(commentsModel);

                                etAddComment.setText("");
                                etAddComment.clearFocus();
                            }
                        });
                    } else {
                        Toast.makeText(DishDetailsActivity.this, "Empty Comment Not Allowed", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    showDialog();
                    etAddComment.setText("");
                    etAddComment.clearFocus();
                }
            }
        });

        btnCancelComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etAddComment.setText("");
                etAddComment.clearFocus();
            }
        });
    }

    private void getDishDetails() {
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("Video URL").exists() && dataSnapshot.child("Video Thumbnail").exists()) {
                    model = (new DishModel(
                            dataSnapshot.getKey(),
                            dataSnapshot.child("Dislike").getValue().toString(),
                            dataSnapshot.child("Like").getValue().toString(),
                            dataSnapshot.child("Rate").getValue().toString(),
                            dataSnapshot.child("Description").getValue().toString(),
                            dataSnapshot.child("Ingredients").getValue().toString(),
                            dataSnapshot.child("Procedures").getValue().toString(),
                            dataSnapshot.child("Recipe Name").getValue().toString(),
                            dataSnapshot.child("Report Count").getValue().toString(),
                            dataSnapshot.child("Uploader ID").getValue().toString(),
                            dataSnapshot.child("Uploader Name").getValue().toString(),
                            dataSnapshot.child("Uploader ProfPic").exists() ? dataSnapshot.child("Uploader ProfPic").getValue().toString() : null,
                            dataSnapshot.child("Video Thumbnail").getValue().toString(),
                            dataSnapshot.child("Video URL").getValue().toString(),
                            dataSnapshot.child("View Count").getValue().toString(),
                            (boolean) dataSnapshot.child("Archived").getValue(),
                            (boolean) dataSnapshot.child("Banned").getValue()
                    ));

                    loadDishDetails();

                    initListeners();
                    svMainContainer.setVisibility(View.VISIBLE);
                    pbLoading.setVisibility(View.GONE);

                    getComments();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        categoryRef.addListenerForSingleValueEvent(eventListener);
    }

    private void getComments() {
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        commentsModels.add(new CommentsModel(
                                child.child("message").getValue().toString(),
                                child.child("user_name").getValue().toString(),
                                child.child("user_prof_pic").getValue().toString()
                        ));
                    }
                    if(!commentsModels.isEmpty()) loadComments();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        commentRef.addListenerForSingleValueEvent(eventListener);
    }

    private void loadComments() {
        for(CommentsModel commentModel : commentsModels){
            inflateComment(commentModel);
        }
    }

    private void inflateComment(CommentsModel commentModel){
        View itemComment = getLayoutInflater().inflate(R.layout.item_comment, null);
        TextView tvUserName = itemComment.findViewById(R.id.tvItemComment_UserName);
        TextView tvComment = itemComment.findViewById(R.id.tvItemComment_Comment);
        CircleImageView ivUserProfPic = itemComment.findViewById(R.id.ivItemComment_UserProfPic);

        tvUserName.setText(commentModel.getUserName());
        tvComment.setText(commentModel.getMessage());
        Glide.with(DishDetailsActivity.this).load(commentModel.getUserProfPic())
                .placeholder(R.drawable.ic_person)
                .into(ivUserProfPic);

        llCommentContainer.addView(itemComment);
    }

    private void loadDishDetails(){
        if (currentUser != null) {
            //Add to History
            historyRef = database.getReference("History").child(currentUser.getUid()).push();
            historyRef.child("Video Key").setValue(key);
            historyRef.child("Category").setValue(category);

            actionsRef = database.getReference("Actions").child(currentUser.getUid()).child(category).child(model.getKey());
            getUserViewDetails();

            if(currentUser.getUid().equals(model.getUploaderID())){
                llRating.setVisibility(View.GONE);
                llReport.setVisibility(View.GONE);
            }
        }

        tvRecipeName.setText(model.getRecipeName());
        tvUploaderName.setText(model.getUploaderName());
        tvViews.setText(model.getViewCount());
        tvLikes.setText(model.getLike());
        tvDislikes.setText(model.getDislike());
        tvDescription.setText(model.getDescription());
        tvIngredients.setText(model.getIngredients());
        tvProcedures.setText(model.getProcedures());
        rbRating.setRating(Float.parseFloat(model.getRate()));
        Glide.with(getApplicationContext()).load(model.getUploaderProfPic()).placeholder(R.drawable.ic_person).into(ivUploaderProfPic);

        llReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentUser!=null) {
                    showReportDialog();
                }
                else showDialog();
            }
        });

        setUpVideoView();
    }

    private void loadUserActions() {
        if (userViewDetailModel.getIsLiked())
            ImageViewCompat.setImageTintList(ivLike, ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
        else
            ImageViewCompat.setImageTintList(ivLike, ColorStateList.valueOf(getResources().getColor(R.color.android_dark_gray)));
        if (userViewDetailModel.getIsDisliked())
            ImageViewCompat.setImageTintList(ivDislike, ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
        else
            ImageViewCompat.setImageTintList(ivDislike, ColorStateList.valueOf(getResources().getColor(R.color.android_dark_gray)));

        if(userViewDetailModel.getReported()) llReport.setVisibility(View.GONE);

        rbRating.setRating(Float.parseFloat(userViewDetailModel.getRating()));
    }

    private void showReportDialog() {
        AlertDialog.Builder alerDialog = new AlertDialog.Builder(this);
        alerDialog.setTitle("Report Video?");
        alerDialog.setIcon(R.drawable.ic_report);
        alerDialog.setMessage("Are you sure you want to report this video because it contains illicit or illigal activities?");
        alerDialog.setCancelable(false);

        alerDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                categoryRef.child("Report Count").setValue(((Integer.parseInt(model.getReportCount())) + 1)+"");
                model.setReportCount(((Integer.parseInt(model.getReportCount())) + 1)+"");
                userViewDetailModel.setReported(true);
                actionsRef.child("isReported").setValue(true);
                llReport.setVisibility(View.GONE);

                if((Integer.parseInt(model.getReportCount()))==3){
                    managementRef = database.getReference("Management").push();
                    managementRef.child("Video Key").setValue(key);
                    managementRef.child("Category").setValue(category);
                    managementRef.child("Status").setValue("Active");
                }
                dialog.dismiss();
            }
        });

        alerDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alerDialog.show();
    }

    private void getUserViewDetails() {
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    actionsRef.child("Rating").setValue(0 + "");
                    actionsRef.child("isDisliked").setValue(false);
                    actionsRef.child("isLiked").setValue(false);
                    actionsRef.child("isReported").setValue(false);

                    getUserViewDetails();
                } else {
                    userViewDetailModel = new UserViewDetailModel(
                            dataSnapshot.child("Rating").getValue().toString(),
                            (boolean) dataSnapshot.child("isDisliked").getValue(),
                            (boolean) dataSnapshot.child("isLiked").getValue(),
                            (boolean) dataSnapshot.child("isReported").getValue());

                    loadUserActions();
                    initListeners();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        actionsRef.addListenerForSingleValueEvent(eventListener);
    }

    private void initListeners() {
        rbRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (ratingBarInitialized) {
                    if (currentUser != null) {
                        actionsRef.child("Rating").setValue(rating + "");
                        userViewDetailModel.setRating(rating + "");
                    } else showDialog();
                }
                ratingBarInitialized = true;
            }
        });

        ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentUser != null) {
                    if (!userViewDetailModel.getIsLiked()) {
                        if (userViewDetailModel.getIsDisliked()) {
                            actionsRef.child("isDisliked").setValue(false);
                            userViewDetailModel.setIsDisliked(false);
                            model.setDislike((Integer.parseInt(model.getDislike()) - 1) + "");
                            tvDislikes.setText(model.getDislike());
                        }
                        actionsRef.child("isLiked").setValue(true);
                        userViewDetailModel.setIsLiked(true);
                        model.setLike((Integer.parseInt(model.getLike()) + 1) + "");
                        tvLikes.setText(model.getLike());
                        loadUserActions();

                        categoryRef.child("Like").setValue(model.getLike());
                        categoryRef.child("Dislike").setValue(model.getDislike());
                    } else {
                        actionsRef.child("isLiked").setValue(false);
                        userViewDetailModel.setIsLiked(false);
                        model.setLike((Integer.parseInt(model.getLike()) - 1) + "");
                        tvLikes.setText(model.getLike());
                        loadUserActions();

                        categoryRef.child("Like").setValue(model.getLike());
                        categoryRef.child("Dislike").setValue(model.getDislike());
                    }
                } else showDialog();
            }
        });

        ivDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentUser != null) {
                    if (!userViewDetailModel.getIsDisliked()) {
                        if (userViewDetailModel.getIsLiked()) {
                            actionsRef.child("isLiked").setValue(false);
                            userViewDetailModel.setIsLiked(false);
                            model.setLike((Integer.parseInt(model.getLike()) - 1) + "");
                            tvLikes.setText(model.getLike());
                        }
                        actionsRef.child("isDisliked").setValue(true);
                        userViewDetailModel.setIsDisliked(true);
                        model.setDislike((Integer.parseInt(model.getDislike()) + 1) + "");
                        tvDislikes.setText(model.getDislike());
                        loadUserActions();

                        categoryRef.child("Like").setValue(model.getLike());
                        categoryRef.child("Dislike").setValue(model.getDislike());
                    } else {
                        actionsRef.child("isDisliked").setValue(false);
                        userViewDetailModel.setIsDisliked(false);
                        model.setDislike((Integer.parseInt(model.getDislike()) - 1) + "");
                        tvDislikes.setText(model.getDislike());
                        loadUserActions();

                        categoryRef.child("Like").setValue(model.getLike());
                        categoryRef.child("Dislike").setValue(model.getDislike());
                    }
                } else showDialog();
            }
        });
    }

    private void showDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DishDetailsActivity.this);
        alertDialog.setTitle("Can't use this feature");
        alertDialog.setMessage("You need to be signed in to use this feature.");
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void setUpVideoView() {
        TrackSelector trackSelector = new DefaultTrackSelector();
        exoPlayer = ExoPlayerFactory.newSimpleInstance(DishDetailsActivity.this, trackSelector);
        vvVideo.setPlayer(exoPlayer);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(DishDetailsActivity.this, Util.getUserAgent(DishDetailsActivity.this, "VideoPlayer"));
        //MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse("https://goo.gl/PyKasq"));
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(model.getVideoURL()));
        exoPlayer.prepare(videoSource);
        exoPlayer.seekTo(currentPlaybackPosition);
        exoPlayer.setPlayWhenReady(false);
        vvVideo.hideController();
        exoPlayer.addListener(new Player.DefaultEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {
                    case Player.STATE_BUFFERING:
                        if (pbBuffering.getVisibility() == View.GONE) {
                            pbBuffering.setVisibility(View.VISIBLE);
                        }
                        //Toast.makeText(DishDetailsActivity.this, "Buffering "+ (pbBuffering.getVisibility()==View.VISIBLE?"Visible":"Gone"), Toast.LENGTH_SHORT).show();
                        break;
                    case Player.STATE_ENDED:
                        vvVideo.hideController();
                        isRepeated = true;
                        ivPlay.setImageResource(R.drawable.ic_replay);
                        ivPlay.setVisibility(View.VISIBLE);
                        break;
                    case Player.STATE_IDLE:
                        ivPlay.setVisibility(View.VISIBLE);
                        vvVideo.hideController();
                        break;
                    case Player.STATE_READY:
                        pbBuffering.setVisibility(View.GONE);
                        vvVideo.hideController();
                        //Toast.makeText(DishDetailsActivity.this, "Ready "+ (pbBuffering.getVisibility()==View.VISIBLE?"Visible":"Gone"), Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
            }

            /*@Override
            public void onPlayerError(ExoPlaybackException error) {
                super.onPlayerError(error);
                vvVideo.hideController();
            }*/
        });

        if(mFullScreenDialog==null) initFullscreenDialog();
        initFullscreenButton();
    }

    private void initFullscreenDialog() {
        Log.e("Dish Details", "closeFullscreenDialog: " + "init dialog");
        mFullScreenDialog = new Dialog(DishDetailsActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (isFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }

    private void openFullscreenDialog() {
        Log.e("Dish Details", "closeFullscreenDialog: " + "open dialog");
        ((ViewGroup) vvVideo.getParent()).removeView(vvVideo);
        ((ViewGroup) pbBuffering.getParent()).removeView(pbBuffering);
        ((ViewGroup) ivPlay.getParent()).removeView(ivPlay);
        mFullScreenDialog.addContentView(vvVideo, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mFullScreenDialog.addContentView(pbBuffering, pbBuffering.getLayoutParams());
        mFullScreenDialog.addContentView(ivPlay, ivPlay.getLayoutParams());
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(DishDetailsActivity.this, R.drawable.ic_fullscreen_skrink));
        isFullscreen = true;
        mFullScreenDialog.show();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    private void closeFullscreenDialog() {
        Log.e("Dish Details", "closeFullscreenDialog: " + "close dialog");
        ((ViewGroup) vvVideo.getParent()).removeView(vvVideo);
        ((ViewGroup) pbBuffering.getParent()).removeView(pbBuffering);
        ((ViewGroup) ivPlay.getParent()).removeView(ivPlay);
        ((FrameLayout) findViewById(R.id.main_media_frame)).addView(vvVideo);
        ((FrameLayout) findViewById(R.id.main_media_frame)).addView(pbBuffering);
        ((FrameLayout) findViewById(R.id.main_media_frame)).addView(ivPlay);
        isFullscreen = false;
        mFullScreenDialog.dismiss();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(DishDetailsActivity.this, R.drawable.ic_fullscreen_expand));
    }

    private void initFullscreenButton() {
        Log.e("Dish Details", "closeFullscreenDialog: " + "init buttons");
        PlaybackControlView controlView = vvVideo.findViewById(R.id.exo_controller);
        controlView.setRewindIncrementMs(5000);
        controlView.setFastForwardIncrementMs(5000);
        mFullScreenIcon = controlView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenButton = controlView.findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFullscreen)
                    openFullscreenDialog();
                else
                    closeFullscreenDialog();
            }
        });
    }

    @Override
    protected void onResume() {
        if(exoPlayer==null) {
            if(model!=null) {
                setUpVideoView();
            }
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if(exoPlayer!=null) {
            currentPlaybackPosition = exoPlayer.getContentPosition();
            exoPlayer.release();
            exoPlayer=null;
        }
        super.onPause();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
