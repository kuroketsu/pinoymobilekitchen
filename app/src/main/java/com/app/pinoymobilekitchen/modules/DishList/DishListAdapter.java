package com.app.pinoymobilekitchen.modules.DishList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pinoymobilekitchen.DishModel;
import com.app.pinoymobilekitchen.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DishListAdapter extends RecyclerView.Adapter<DishListAdapter.MyViewHolder> {

    private Context context;
    private List<DishModel> models;
    private String category;
    private ItemClickListener itemClickListener;

    public DishListAdapter(Context context, List<DishModel> models, String category, ItemClickListener itemClickListener) {
        this.context = context;
        this.models = models;
        this.category = category;
        this.itemClickListener = itemClickListener;
    }

    public void setModelList(List<DishModel> models) {
        this.models = models;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DishListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dish, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DishListAdapter.MyViewHolder holder, final int position) {
        holder.tvRecipeName.setText(models.get(position).getRecipeName());
        holder.tvUploaderName.setText(models.get(position).getUploaderName());
        holder.tvViewCount.setText(models.get(position).getViewCount());
        Glide.with(context).load(models.get(position).getVideoThumbnail())
                .into(holder.ivThumbnail);
        Glide.with(context).load(models.get(position).getUploaderProfPic())
                .placeholder(R.drawable.ic_person)
                .into(holder.ivProfPic);
        holder.rlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClicked(models.get(position).getKey(), category);
            }
        });
    }

    public interface ItemClickListener{
        void onItemClicked(String key, String category);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvRecipeName, tvUploaderName, tvViewCount;
        ImageView ivThumbnail;
        CircleImageView ivProfPic;
        LinearLayout rlContainer;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvRecipeName = itemView.findViewById(R.id.tvDishItem_RecipeName);
            tvUploaderName = itemView.findViewById(R.id.tvDishItem_UploaderName);
            tvViewCount = itemView.findViewById(R.id.tvDishItem_ViewCount);
            ivThumbnail = itemView.findViewById(R.id.ivDishItem_Image);
            ivProfPic = itemView.findViewById(R.id.ivDishItem_ProfPic);
            rlContainer = itemView.findViewById(R.id.rlDishItem_Container);
        }
    }
}
