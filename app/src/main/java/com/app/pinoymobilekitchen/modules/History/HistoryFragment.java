package com.app.pinoymobilekitchen.modules.History;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.app.pinoymobilekitchen.DishModel;
import com.app.pinoymobilekitchen.MainActivity;
import com.app.pinoymobilekitchen.R;
import com.app.pinoymobilekitchen.modules.DishDetails.DishDetailsActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment implements HistoryAdapter.ItemClickListener {

    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    private View v;
    private RecyclerView recyclerView;
    private ProgressBar pbLoading;
    private List<DishModel> historyModels;
    private HistoryAdapter adapter;

    private FirebaseDatabase database;

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private int historyCount;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        //SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            //searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    if(adapter!=null){
                        String userInput = newText.toLowerCase();
                        List<DishModel> newModels = new ArrayList<>();
                        for(DishModel model : historyModels){
                            if(model.getRecipeName().toLowerCase().contains(userInput)){
                                newModels.add(model);
                            }
                        }
                        adapter.setModelList(newModels);
                        return true;
                    }

                    return false;
                }
                @Override
                public boolean onQueryTextSubmit(String query) {

                    return false;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_history, container, false);

        recyclerView = v.findViewById(R.id.rvHistory);
        pbLoading = v.findViewById(R.id.pbHistory_Loading);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        //init firebase
        database = FirebaseDatabase.getInstance();

        getHistory();

        return v;
    }

    private void getHistory() {
        pbLoading.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

        historyCount = 0;
        historyModels = new ArrayList<>();
        DatabaseReference libraryRef = database.getReference("History").child(currentUser.getUid());
        libraryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    int count = 0;
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        historyModels.add(new DishModel());
                        getDishDetails(child.child("Category").getValue().toString(), child.child("Video Key").getValue().toString(), count);
                        //Log.e("Library", "after loop: model size="+libraryModels.size()+", expected count="+libraryCount);
                        count++;
                    }
                }
                else {
                    pbLoading.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getDishDetails(final String category, String videoKey, final int index) {
        DatabaseReference categoryRef = database.getReference("Category").child(category).child(videoKey);

        categoryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DishModel model = (new DishModel(
                        category,
                        dataSnapshot.getKey(),
                        dataSnapshot.child("Dislike").getValue().toString(),
                        dataSnapshot.child("Like").getValue().toString(),
                        dataSnapshot.child("Rate").getValue().toString(),
                        dataSnapshot.child("Description").getValue().toString(),
                        dataSnapshot.child("Ingredients").getValue().toString(),
                        dataSnapshot.child("Procedures").getValue().toString(),
                        dataSnapshot.child("Recipe Name").getValue().toString(),
                        dataSnapshot.child("Report Count").getValue().toString(),
                        dataSnapshot.child("Uploader ID").getValue().toString(),
                        dataSnapshot.child("Uploader Name").getValue().toString(),
                        dataSnapshot.child("Uploader ProfPic").exists() ? dataSnapshot.child("Uploader ProfPic").getValue().toString() : null,
                        dataSnapshot.child("Video Thumbnail").getValue().toString(),
                        dataSnapshot.child("Video URL").getValue().toString(),
                        dataSnapshot.child("View Count").getValue().toString(),
                        (boolean) dataSnapshot.child("Archived").getValue(),
                        (boolean) dataSnapshot.child("Banned").getValue()
                ));
                if (!model.getBanned()) {
                    historyModels.set(index, model);
                    historyCount+=1;
                }
                //Log.e("Library", "getDishDetails: model size="+libraryModels.size()+", expected count="+libraryCount);

                if(historyModels.size()==historyCount) {
                    Collections.reverse(historyModels);
                    adapter = new HistoryAdapter(getActivity(), historyModels, HistoryFragment.this);
                    recyclerView.setAdapter(adapter);
                    pbLoading.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onItemClicked(String key, String category) {
        Intent intent = new Intent(getActivity(), DishDetailsActivity.class);
        intent.putExtra("category", category);
        intent.putExtra("key", key);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            getHistory();
        }
    }

    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle("History");

    }
}
