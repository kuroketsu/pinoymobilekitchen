package com.app.pinoymobilekitchen.modules.DishList;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.app.pinoymobilekitchen.DishModel;
import com.app.pinoymobilekitchen.MainActivity;
import com.app.pinoymobilekitchen.R;
import com.app.pinoymobilekitchen.modules.DishDetails.DishDetailsActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DishListFragment extends Fragment implements DishListAdapter.ItemClickListener {

    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    private View v;
    private RecyclerView recyclerView;
    private List<DishModel> models;
    private ProgressBar pbLoading;
    private LinearLayout llEmpty;

    private FirebaseDatabase database;
    private DatabaseReference categoryRef;
    private ValueEventListener eventListener;
    private DishListAdapter adapter;

    public static DishListFragment newInstance(String category) {
        Bundle args = new Bundle();
        DishListFragment fragment = new DishListFragment();
        args.putString("category", category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        //SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            //searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    if(adapter!=null){
                        String userInput = newText.toLowerCase();
                        List<DishModel> newModels = new ArrayList<>();
                        for(DishModel model : models){
                            if(model.getRecipeName().toLowerCase().contains(userInput)){
                                newModels.add(model);
                            }
                        }
                        adapter.setModelList(newModels);
                        return true;
                    }

                    return false;
                }
                @Override
                public boolean onQueryTextSubmit(String query) {

                    return false;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_dish_list, container, false);

        //init firebase
        database = FirebaseDatabase.getInstance();
        categoryRef = database.getReference("Category").child(getArguments().getString("category"));

        recyclerView = v.findViewById(R.id.rvDishList);
        pbLoading = v.findViewById(R.id.pbDishList_Loading);
        llEmpty = v.findViewById(R.id.llDishList_Empty);

        getDishes();
        return v;
    }

    private void getDishes(){
        pbLoading.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                models = new ArrayList<>();
                for(DataSnapshot child: dataSnapshot.getChildren()) {
                    if(child.child("Video URL").exists() && child.child("Video Thumbnail").exists()) {
                        if(!(boolean)child.child("Archived").getValue() && !(boolean)child.child("Banned").getValue()) {
                            models.add(new DishModel(
                                    child.getKey(),
                                    child.child("Dislike").getValue().toString(),
                                    child.child("Like").getValue().toString(),
                                    child.child("Rate").getValue().toString(),
                                    child.child("Description").getValue().toString(),
                                    child.child("Ingredients").getValue().toString(),
                                    child.child("Procedures").getValue().toString(),
                                    child.child("Recipe Name").getValue().toString(),
                                    child.child("Report Count").getValue().toString(),
                                    child.child("Uploader ID").getValue().toString(),
                                    child.child("Uploader Name").getValue().toString(),
                                    child.child("Uploader ProfPic").exists() ? child.child("Uploader ProfPic").getValue().toString() : null,
                                    child.child("Video Thumbnail").getValue().toString(),
                                    child.child("Video URL").getValue().toString(),
                                    child.child("View Count").getValue().toString(),
                                    (boolean) child.child("Archived").getValue(),
                                    (boolean) child.child("Banned").getValue()
                            ));
                        }
                    }
                    else categoryRef.child(child.getKey()).updateChildren(null);
                }
                adapter = new DishListAdapter(getActivity(), models, getArguments().getString("category"), DishListFragment.this);
                recyclerView.setAdapter(adapter);
                pbLoading.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                if(models.isEmpty()) llEmpty.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        categoryRef.addListenerForSingleValueEvent(eventListener);
    }

    @Override
    public void onItemClicked(String key, String category) {
        Intent intent = new Intent(getActivity(), DishDetailsActivity.class);
        intent.putExtra("category", category);
        intent.putExtra("key", key);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            getDishes();
        }
    }

    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle(getArguments().getString("category").toUpperCase());
    }
}
