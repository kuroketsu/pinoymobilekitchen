package com.app.pinoymobilekitchen.modules.Profile;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.app.pinoymobilekitchen.MainActivity;
import com.app.pinoymobilekitchen.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment {

    View v;
    Button btnSignUp;
    TextInputEditText etFirstName, etLastName, etEmail, etPassword, etConfirmPassword;
    CheckBox cbTerms;

    private FirebaseAuth mAuth;
    private boolean isTermsAgree = false;

    public SignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_sign_up, container, false);

        etFirstName = v.findViewById(R.id.etSignUp_FirstName);
        etLastName = v.findViewById(R.id.etSignUp_LastName);
        etEmail = v.findViewById(R.id.etSignUp_Email);
        etPassword = v.findViewById(R.id.etSignUp_Password);
        etConfirmPassword = v.findViewById(R.id.etSignUp_ConfirmPassword);
        cbTerms = v.findViewById(R.id.cbSignUp_Terms);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        btnSignUp = v.findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etFirstName.getText().toString().isEmpty()
                        || etLastName.getText().toString().isEmpty()
                        || etEmail.getText().toString().isEmpty()
                        || etPassword.getText().toString().isEmpty()
                        || etConfirmPassword.getText().toString().isEmpty())
                {
                    Toast.makeText(getActivity(), "Empty Fields Not Allowed", Toast.LENGTH_SHORT).show();
                }
                else if(!cbTerms.isChecked()){
                    Toast.makeText(getActivity(), "Please Check Our Terms & Conditions", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(etPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
                        signUp();
                    }
                    else {
                        Toast.makeText(getActivity(), "Password Did Not Match", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        cbTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked && !isTermsAgree){
                    cbTerms.setChecked(false);
                    showTermsDialog();
                }
                if(!isChecked && isTermsAgree){
                    isTermsAgree = false;
                }
            }
        });

        return v;
    }

    private void showTermsDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Terms of Service & Agreement");
        alertDialog.setMessage("Terms of Service:\n" +
                "These terms of Service are a contract between the user and the administrator of the My Pinoy Recipe Application. By Using the Pinoy Mobile Kitchen Application and any services accessible from the Application, you are agreeing to be bound by the following terms and conditions (\"Terms of Service\") \n" +
                "\n" +
                "Agreement:\n" +
                "If the uploaded video of the user is reported three times it will be reviewed by the administrator and if it contains bad contents the video will be banned.\n" +
                "\n" +
                "If you do not agree to these Terms and Agreement or any part thereof, your only remedy is to not use the My Pinoy Recipe Application. Violation of any of the terms below will result in Termination.");
        alertDialog.setPositiveButton("Agree", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isTermsAgree = true;
                cbTerms.setChecked(true);
                dialog.dismiss();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void signUp() {
        mAuth.createUserWithEmailAndPassword(etEmail.getText().toString(), etConfirmPassword.getText().toString())
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.e("Sign Up", "createUserWithEmail:success");
                            final FirebaseUser user = mAuth.getCurrentUser();
                            user.updateProfile(new UserProfileChangeRequest.Builder().setDisplayName(etFirstName.getText().toString() + " " + etLastName.getText().toString()).build()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    updateUI(user);
                                }
                            });
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.e("Sign Up", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(getActivity(), "Authentication failed.", Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    private void updateUI(FirebaseUser user) {
        if(user!=null){
            getFragmentManager().beginTransaction().replace(R.id.content_frame, new ProfileFragment()).commit();
        }
    }

    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle("Sign Up");
    }

}
