package com.app.pinoymobilekitchen.modules.Library.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.pinoymobilekitchen.R;
import com.app.pinoymobilekitchen.DishModel;
import com.app.pinoymobilekitchen.modules.Library.activities.EditActivity;
import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class LibraryAdapter extends RecyclerView.Adapter<LibraryAdapter.MyViewHolder> {

    private Context context;
    private List<DishModel> models;
    private ItemClickListener itemClickListener;
    private ArchiveClickListener archiveClickListener;

    public LibraryAdapter(Context context, List<DishModel> models, ItemClickListener itemClickListener, ArchiveClickListener archiveClickListener) {
        this.context = context;
        this.models = models;
        this.itemClickListener = itemClickListener;
        this.archiveClickListener = archiveClickListener;
    }

    public void setModelList(List<DishModel> models) {
        this.models = models;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public LibraryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dish, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final LibraryAdapter.MyViewHolder holder, final int position) {
        holder.tvRecipeName.setText(models.get(position).getRecipeName());
        holder.tvUploaderName.setText(models.get(position).getUploaderName());
        holder.tvViewCount.setText(models.get(position).getViewCount());
        Glide.with(context).load(models.get(position).getVideoThumbnail())
                .into(holder.ivThumbnail);
        Glide.with(context).load(models.get(position).getUploaderProfPic())
                .placeholder(R.drawable.ic_person)
                .into(holder.ivProfPic);
        holder.rlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClicked(models.get(position).getKey(), models.get(position).getCategory());
            }
        });
        holder.tvOptionsMenu.setVisibility(View.VISIBLE);
        holder.tvOptionsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, holder.tvOptionsMenu);
                popup.inflate(R.menu.dish_item_menu);
                popup.getMenu().getItem(0).setVisible(true);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.menu_dish_item_archive) {
                            archiveClickListener.onArchiveClicked(position);
                            return true;
                        }
                        if (item.getItemId() == R.id.menu_dish_item_edit) {
                            Intent intent = new Intent(context, EditActivity.class);
                            intent.putExtra("category", models.get(position).getCategory());
                            intent.putExtra("key", models.get(position).getKey());
                            context.startActivity(intent);
                            return true;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });
    }

    public interface ItemClickListener{
        void onItemClicked(String key, String category);
    }

    public interface ArchiveClickListener{
        void onArchiveClicked(int position);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvRecipeName, tvUploaderName, tvViewCount;
        ImageView ivThumbnail;
        CircleImageView ivProfPic;
        LinearLayout rlContainer;

        TextView tvOptionsMenu;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvRecipeName = itemView.findViewById(R.id.tvDishItem_RecipeName);
            tvUploaderName = itemView.findViewById(R.id.tvDishItem_UploaderName);
            tvViewCount = itemView.findViewById(R.id.tvDishItem_ViewCount);
            ivThumbnail = itemView.findViewById(R.id.ivDishItem_Image);
            ivProfPic = itemView.findViewById(R.id.ivDishItem_ProfPic);
            rlContainer = itemView.findViewById(R.id.rlDishItem_Container);

            tvOptionsMenu = itemView.findViewById(R.id.tvDishItem_Options);
        }
    }
}
