package com.app.pinoymobilekitchen.modules.Management;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.app.pinoymobilekitchen.DishModel;
import com.app.pinoymobilekitchen.MainActivity;
import com.app.pinoymobilekitchen.R;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManagementFragment extends Fragment implements ManagementListAdapter.OptionsClickListener {

    private View v;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private ProgressBar pbLoading;
    private String[] tabs = new String[]{"Active", "Ignored", "Banned"};
    private List<DishModel> activeModels, ignoredModels, bannedModels;

    private FirebaseDatabase database;

    private int activeCount, ignoredCount, bannedCount;

    public ManagementFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_management, container, false);

        tabLayout = v.findViewById(R.id.tlManagement);
        viewPager = v.findViewById(R.id.vpManagement);
        pbLoading = v.findViewById(R.id.pbManagement_Loading);

        //init firebase
        database = FirebaseDatabase.getInstance();

        adapter = new ViewPagerAdapter(getFragmentManager(), tabs, null, null, null, ManagementFragment.this);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(tabs.length-1);
        tabLayout.addTab(tabLayout.newTab().setText(tabs[0]));
        tabLayout.addTab(tabLayout.newTab().setText(tabs[1]));
        tabLayout.addTab(tabLayout.newTab().setText(tabs[2]));
        tabLayout.setupWithViewPager(viewPager, false);

        getManagement();
        return v;
    }

    private void getManagement() {
        pbLoading.setVisibility(View.VISIBLE);

        activeCount = 0;
        ignoredCount = 0;
        bannedCount = 0;

        activeModels = new ArrayList<>();
        ignoredModels = new ArrayList<>();
        bannedModels = new ArrayList<>();

        DatabaseReference managementRef = database.getReference("Management");
        managementRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        String status = child.child("Status").getValue().toString();
                        String managementKey = child.getKey();
                        switch (status) {
                            case "Active":
                                activeCount +=1;
                                break;
                            case "Ignored":
                                ignoredCount +=1;
                                break;
                            case "Banned":
                                bannedCount +=1;
                                break;
                        }
                        getDishDetails(child.child("Category").getValue().toString(), child.child("Video Key").getValue().toString(), status, managementKey);
                        //Log.e("Library", "after loop: model size="+libraryModels.size()+", expected count="+libraryCount);
                    }
                }
                else pbLoading.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getDishDetails(final String category, String videoKey, final String status, final String managementKey) {
        DatabaseReference categoryRef = database.getReference("Category").child(category).child(videoKey);

        categoryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DishModel model = (new DishModel(
                        category,
                        dataSnapshot.getKey(),
                        dataSnapshot.child("Dislike").getValue().toString(),
                        dataSnapshot.child("Like").getValue().toString(),
                        dataSnapshot.child("Rate").getValue().toString(),
                        dataSnapshot.child("Description").getValue().toString(),
                        dataSnapshot.child("Ingredients").getValue().toString(),
                        dataSnapshot.child("Procedures").getValue().toString(),
                        dataSnapshot.child("Recipe Name").getValue().toString(),
                        dataSnapshot.child("Report Count").getValue().toString(),
                        dataSnapshot.child("Uploader ID").getValue().toString(),
                        dataSnapshot.child("Uploader Name").getValue().toString(),
                        dataSnapshot.child("Uploader ProfPic").exists() ? dataSnapshot.child("Uploader ProfPic").getValue().toString() : null,
                        dataSnapshot.child("Video Thumbnail").getValue().toString(),
                        dataSnapshot.child("Video URL").getValue().toString(),
                        dataSnapshot.child("View Count").getValue().toString(),
                        (boolean) dataSnapshot.child("Archived").getValue(),
                        (boolean) dataSnapshot.child("Banned").getValue(),
                        managementKey
                ));
                switch (status) {
                    case "Active":
                        activeModels.add(model);
                        break;
                    case "Ignored":
                        ignoredModels.add(model);
                        break;
                    case "Banned":
                        bannedModels.add(model);
                        break;
                }

                if(activeModels.size()==activeCount && ignoredModels.size()==ignoredCount && bannedModels.size()==bannedCount) {
                    pbLoading.setVisibility(View.GONE);
                    setViewPagerAdapter();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setViewPagerAdapter(){
        adapter = new ViewPagerAdapter(getFragmentManager(), tabs, activeModels, ignoredModels, bannedModels, ManagementFragment.this);
        viewPager.setAdapter(adapter);
    }

    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle("Management");
    }

    @Override
    public void onOptionsClicked(String key, String category, String action, String managementKey) {
        DatabaseReference categoryRef = database.getReference("Category").child(category).child(key);
        DatabaseReference managementRef = database.getReference("Management").child(managementKey);
        switch (action){
            case "Ignored":
                categoryRef.child("Banned").setValue(false);
                break;
            case "Banned":
                categoryRef.child("Banned").setValue(true);
                break;
            case "Active":
                categoryRef.child("Banned").setValue(false);
                break;
        }
        managementRef.child("Status").setValue(action);

        setViewPagerAdapter();

        getManagement();
    }

}
