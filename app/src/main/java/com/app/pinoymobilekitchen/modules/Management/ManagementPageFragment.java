package com.app.pinoymobilekitchen.modules.Management;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.app.pinoymobilekitchen.DishModel;
import com.app.pinoymobilekitchen.R;
import com.app.pinoymobilekitchen.modules.DishDetails.DishDetailsActivity;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManagementPageFragment extends Fragment implements ManagementListAdapter.ItemClickListener {

    private View v;
    private RecyclerView recyclerView;
    private ManagementListAdapter adapter;

    private List<DishModel> models;
    private String status;
    private ManagementListAdapter.OptionsClickListener optionsClickListener;

    private void setModels(List<DishModel> models) {
        this.models = models;
    }
    private void setStatus(String status) {
        this.status = status;
    }

    public void setOptionsClickListener(ManagementListAdapter.OptionsClickListener optionsClickListener) {
        this.optionsClickListener = optionsClickListener;
    }


    public static ManagementPageFragment newInstance(List<DishModel> models, String status, ManagementListAdapter.OptionsClickListener optionsClickListener) {
        ManagementPageFragment fragment = new ManagementPageFragment();
        fragment.setModels(models);
        fragment.setStatus(status);
        fragment.setOptionsClickListener(optionsClickListener);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_management_page, container, false);

        recyclerView = v.findViewById(R.id.rvManagement);

        if(models!=null){
            Log.e("Management Page", status + " not null");
            if(models.size()!=0) {
                Log.e("Management Page", status + " model size=" + models.size());
                adapter = new ManagementListAdapter(getActivity(), models, status, ManagementPageFragment.this, optionsClickListener);
                recyclerView.setAdapter(adapter);
            }
        }
        else{
            Log.e("Management Page", status + " null");
        }

        return v;
    }

    @Override
    public void onItemClicked(String key, String category) {
        Intent intent = new Intent(getActivity(), DishDetailsActivity.class);
        intent.putExtra("category", category);
        intent.putExtra("key", key);
        startActivityForResult(intent, 1);
    }
}
