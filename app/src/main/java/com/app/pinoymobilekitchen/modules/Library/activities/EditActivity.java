package com.app.pinoymobilekitchen.modules.Library.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;

import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.pinoymobilekitchen.DishModel;
import com.app.pinoymobilekitchen.R;
import com.app.pinoymobilekitchen.modules.DishDetails.DishDetailsActivity;
import com.app.pinoymobilekitchen.modules.DishDetails.UserViewDetailModel;
import com.app.pinoymobilekitchen.modules.Library.UploadActivity;
import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditActivity extends AppCompatActivity {

    PlayerView vvVideo;
    TextView tvRecipeName, tvUploaderName, tvViews, tvLikes, tvDislikes, tvIngredients, tvProcedures;
    ImageView ivLike, ivDislike;
    CircleImageView ivUploaderProfPic;
    SimpleExoPlayer exoPlayer;
    ScrollView svMainContainer;
    ProgressBar pbLoading;
    Button btnSave;
    EditText etDescription;

    private FirebaseDatabase database;
    private DatabaseReference categoryRef;

    DishModel model;
    String key;
    String category;

    private boolean isFullscreen = false;
    private FrameLayout mFullScreenButton;
    private ImageView mFullScreenIcon;
    Dialog mFullScreenDialog;
    private long currentPlaybackPosition = 0;
    private ProgressBar pbBuffering;
    private ImageView ivPlay;
    private boolean isRepeated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        key = getIntent().getStringExtra("key");
        category = getIntent().getStringExtra("category");

        vvVideo = findViewById(R.id.vvEdit_Video);
        tvRecipeName = findViewById(R.id.tvEdit_RecipeName);
        tvUploaderName = findViewById(R.id.tvEdit_UploaderName);
        tvViews = findViewById(R.id.tvEdit_Views);
        ivUploaderProfPic = findViewById(R.id.ivEdit_UploaderProfPic);
        tvLikes = findViewById(R.id.tvEdit_Like);
        tvDislikes = findViewById(R.id.tvEdit_Dislike);
        ivLike = findViewById(R.id.ivEdit_Like);
        ivDislike = findViewById(R.id.ivEdit_Dislike);
        tvIngredients = findViewById(R.id.tvEdit_Ingredients);
        tvProcedures = findViewById(R.id.tvEdit_Procedures);
        svMainContainer = findViewById(R.id.svEdit_MainContainer);
        pbLoading = findViewById(R.id.pbEdit_Loading);
        pbBuffering = findViewById(R.id.pbEdit_Buffering);
        ivPlay = findViewById(R.id.ivEdit_Play);
        btnSave = findViewById(R.id.btnEdit_Save);
        etDescription = findViewById(R.id.etEdit_Description);


        database = FirebaseDatabase.getInstance();

        categoryRef = database.getReference("Category").child(category).child(key);

        getDishDetails();

        ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (exoPlayer != null) {
                    if (isRepeated) {
                        exoPlayer.seekTo(0);
                    }
                    exoPlayer.setPlayWhenReady(true);
                    ivPlay.setVisibility(View.GONE);
                    ivPlay.setImageResource(R.drawable.ic_play);

                    if (model != null)
                        categoryRef.child("View Count").setValue(Integer.parseInt(model.getViewCount()) + 1 + "");
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etDescription.getText().toString().isEmpty()) {
                    categoryRef.child("Description").setValue(etDescription.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(EditActivity.this, "Saved!", Toast.LENGTH_LONG).show();
                        }
                    });
                }
                else {
                    Toast.makeText(EditActivity.this, "Description cannot be empty", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void getDishDetails() {
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("Video URL").exists() && dataSnapshot.child("Video Thumbnail").exists()) {
                    model = (new DishModel(
                            dataSnapshot.getKey(),
                            dataSnapshot.child("Dislike").getValue().toString(),
                            dataSnapshot.child("Like").getValue().toString(),
                            dataSnapshot.child("Rate").getValue().toString(),
                            dataSnapshot.child("Description").getValue().toString(),
                            dataSnapshot.child("Ingredients").getValue().toString(),
                            dataSnapshot.child("Procedures").getValue().toString(),
                            dataSnapshot.child("Recipe Name").getValue().toString(),
                            dataSnapshot.child("Report Count").getValue().toString(),
                            dataSnapshot.child("Uploader ID").getValue().toString(),
                            dataSnapshot.child("Uploader Name").getValue().toString(),
                            dataSnapshot.child("Uploader ProfPic").exists() ? dataSnapshot.child("Uploader ProfPic").getValue().toString() : null,
                            dataSnapshot.child("Video Thumbnail").getValue().toString(),
                            dataSnapshot.child("Video URL").getValue().toString(),
                            dataSnapshot.child("View Count").getValue().toString(),
                            (boolean) dataSnapshot.child("Archived").getValue(),
                            (boolean) dataSnapshot.child("Banned").getValue()
                    ));

                    loadDishDetails();

                    svMainContainer.setVisibility(View.VISIBLE);
                    pbLoading.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        categoryRef.addListenerForSingleValueEvent(eventListener);
    }

    private void loadDishDetails() {
        tvRecipeName.setText(model.getRecipeName());
        tvUploaderName.setText(model.getUploaderName());
        tvViews.setText(model.getViewCount());
        tvLikes.setText(model.getLike());
        tvDislikes.setText(model.getDislike());
        etDescription.setText(model.getDescription());
        tvIngredients.setText(model.getIngredients());
        tvProcedures.setText(model.getProcedures());
        Glide.with(getApplicationContext()).load(model.getUploaderProfPic()).placeholder(R.drawable.ic_person).into(ivUploaderProfPic);

        setUpVideoView();
    }

    private void setUpVideoView() {
        TrackSelector trackSelector = new DefaultTrackSelector();
        exoPlayer = ExoPlayerFactory.newSimpleInstance(EditActivity.this, trackSelector);
        vvVideo.setPlayer(exoPlayer);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(EditActivity.this, Util.getUserAgent(EditActivity.this, "VideoPlayer"));
        //MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse("https://goo.gl/PyKasq"));
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(model.getVideoURL()));
        exoPlayer.prepare(videoSource);
        exoPlayer.seekTo(currentPlaybackPosition);
        exoPlayer.setPlayWhenReady(false);
        vvVideo.hideController();
        exoPlayer.addListener(new Player.DefaultEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {
                    case Player.STATE_BUFFERING:
                        if (pbBuffering.getVisibility() == View.GONE) {
                            pbBuffering.setVisibility(View.VISIBLE);
                        }
                        //Toast.makeText(DishDetailsActivity.this, "Buffering "+ (pbBuffering.getVisibility()==View.VISIBLE?"Visible":"Gone"), Toast.LENGTH_SHORT).show();
                        break;
                    case Player.STATE_ENDED:
                        vvVideo.hideController();
                        isRepeated = true;
                        ivPlay.setImageResource(R.drawable.ic_replay);
                        ivPlay.setVisibility(View.VISIBLE);
                        break;
                    case Player.STATE_IDLE:
                        ivPlay.setVisibility(View.VISIBLE);
                        vvVideo.hideController();
                        break;
                    case Player.STATE_READY:
                        pbBuffering.setVisibility(View.GONE);
                        vvVideo.hideController();
                        //Toast.makeText(DishDetailsActivity.this, "Ready "+ (pbBuffering.getVisibility()==View.VISIBLE?"Visible":"Gone"), Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
            }

            /*@Override
            public void onPlayerError(ExoPlaybackException error) {
                super.onPlayerError(error);
                vvVideo.hideController();
            }*/
        });

        if (mFullScreenDialog == null) initFullscreenDialog();
        initFullscreenButton();
    }

    private void initFullscreenDialog() {
        Log.e("Dish Details", "closeFullscreenDialog: " + "init dialog");
        mFullScreenDialog = new Dialog(EditActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (isFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }

    private void openFullscreenDialog() {
        Log.e("Dish Details", "closeFullscreenDialog: " + "open dialog");
        ((ViewGroup) vvVideo.getParent()).removeView(vvVideo);
        ((ViewGroup) pbBuffering.getParent()).removeView(pbBuffering);
        ((ViewGroup) ivPlay.getParent()).removeView(ivPlay);
        mFullScreenDialog.addContentView(vvVideo, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mFullScreenDialog.addContentView(pbBuffering, pbBuffering.getLayoutParams());
        mFullScreenDialog.addContentView(ivPlay, ivPlay.getLayoutParams());
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(EditActivity.this, R.drawable.ic_fullscreen_skrink));
        isFullscreen = true;
        mFullScreenDialog.show();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    private void closeFullscreenDialog() {
        Log.e("Dish Details", "closeFullscreenDialog: " + "close dialog");
        ((ViewGroup) vvVideo.getParent()).removeView(vvVideo);
        ((ViewGroup) pbBuffering.getParent()).removeView(pbBuffering);
        ((ViewGroup) ivPlay.getParent()).removeView(ivPlay);
        ((FrameLayout) findViewById(R.id.main_media_frame)).addView(vvVideo);
        ((FrameLayout) findViewById(R.id.main_media_frame)).addView(pbBuffering);
        ((FrameLayout) findViewById(R.id.main_media_frame)).addView(ivPlay);
        isFullscreen = false;
        mFullScreenDialog.dismiss();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(EditActivity.this, R.drawable.ic_fullscreen_expand));
    }

    private void initFullscreenButton() {
        Log.e("Dish Details", "closeFullscreenDialog: " + "init buttons");
        PlaybackControlView controlView = vvVideo.findViewById(R.id.exo_controller);
        controlView.setRewindIncrementMs(5000);
        controlView.setFastForwardIncrementMs(5000);
        mFullScreenIcon = controlView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenButton = controlView.findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFullscreen)
                    openFullscreenDialog();
                else
                    closeFullscreenDialog();
            }
        });
    }

    @Override
    protected void onResume() {
        if (exoPlayer == null) {
            if (model != null) {
                setUpVideoView();
            }
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (exoPlayer != null) {
            currentPlaybackPosition = exoPlayer.getContentPosition();
            exoPlayer.release();
            exoPlayer = null;
        }
        super.onPause();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
