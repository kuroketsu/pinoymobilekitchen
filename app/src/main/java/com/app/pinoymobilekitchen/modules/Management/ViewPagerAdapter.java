package com.app.pinoymobilekitchen.modules.Management;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.app.pinoymobilekitchen.DishModel;

import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private String[] tabs;
    private List<DishModel> activeModels, ignoredModels, bannedModels;
    private ManagementListAdapter.OptionsClickListener optionsClickListener;

    public ViewPagerAdapter(@NonNull FragmentManager fm, String[] tabs, List<DishModel> activeModels, List<DishModel> ignoredModels, List<DishModel> bannedModels, ManagementListAdapter.OptionsClickListener optionsClickListener) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT );
        this.tabs = tabs;
        this.activeModels = activeModels;
        this.ignoredModels = ignoredModels;
        this.bannedModels = bannedModels;
        this.optionsClickListener = optionsClickListener;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return ManagementPageFragment.newInstance(activeModels, tabs[position], optionsClickListener);
            case 1:
                return ManagementPageFragment.newInstance(ignoredModels, tabs[position], optionsClickListener);
            case 2:
                return ManagementPageFragment.newInstance(bannedModels, tabs[position], optionsClickListener);
        }
        return null;
    }

    @Override
    public int getCount() {
        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }
}
