package com.app.pinoymobilekitchen.modules.Profile;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.pinoymobilekitchen.MainActivity;
import com.app.pinoymobilekitchen.R;
import com.app.pinoymobilekitchen.modules.History.HistoryFragment;
import com.app.pinoymobilekitchen.modules.Management.ManagementFragment;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    private View v;
    private TextView tvName, tvEmail;
    private Button btnSignOut;
    private CircleImageView ivProfPic;
    private RelativeLayout rlHistory, rlChangePass, rlManagement;
    private int REQUEST_CODE_IMAGE = 4;
    private String imageFilePath;
    private Uri imageUri;
    private Bitmap imgbitmap;

    private FirebaseAuth mAuth;
    FirebaseUser currentUser;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void updateUI(FirebaseUser currentUser) {
        if(currentUser!=null){
            tvName.setText(currentUser.getDisplayName());
            tvEmail.setText(currentUser.getEmail());
            Glide.with(getActivity()).load(currentUser.getPhotoUrl()).placeholder(R.drawable.ic_person).into(ivProfPic);

            if(currentUser.getEmail().equals("pinoymobilekitchenapp@yahoo.com")){
                rlManagement.setVisibility(View.VISIBLE);
            }
        }
        else {
            getFragmentManager().beginTransaction().replace(R.id.content_frame, new SignInFragment()).commit();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_profile, container, false);

        tvName = v.findViewById(R.id.tvProfile_Name);
        tvEmail = v.findViewById(R.id.tvProfile_Email);
        btnSignOut = v.findViewById(R.id.btnProfile_SignOut);
        ivProfPic = v.findViewById(R.id.ivProfile_ProfPic);
        rlHistory = v.findViewById(R.id.rlProfile_History);
        rlChangePass = v.findViewById(R.id.rlProfile_ChangePass);
        rlManagement = v.findViewById(R.id.rlProfile_ReportManagement);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                getFragmentManager().beginTransaction().replace(R.id.content_frame, new SignInFragment()).commit();
            }
        });

        ivProfPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]
                            {
                                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                            }, REQUEST_CODE_IMAGE);
                }
                else {
                    selectImage();
                }
            }
        });

        rlChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame, new ChangePassFragment()).addToBackStack(null).commit();
            }
        });

        rlHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame, new HistoryFragment()).addToBackStack(null).commit();
            }
        });

        rlManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame, new ManagementFragment()).addToBackStack(null).commit();
            }
        });

        return v;
    }

    private void selectImage() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == REQUEST_CODE_IMAGE && resultCode == RESULT_OK && data != null){
            imageUri = data.getData();
            uploadProfPic();
            /*try{
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().getContentResolver().query(imageUri, filePathColumn, null, null, null);
                if (cursor.moveToFirst()) {
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageFilePath = cursor.getString(columnIndex);
                    imgbitmap = BitmapFactory.decodeFile(imageFilePath);
                }
                cursor.close();
                uploadProfPic();
            }
            catch (Exception ex){
                ex.printStackTrace();
            }*/
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void uploadProfPic() {
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setPhotoUri(imageUri)
                .build();

        currentUser.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            if(isAdded()) {
                                Toast.makeText(getActivity(), "Profile Picture Updated", Toast.LENGTH_SHORT).show();
                                Glide.with(getActivity()).load(currentUser.getPhotoUrl()).placeholder(R.drawable.ic_person).into(ivProfPic);
                            }
                        }
                    }
                });
    }

    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle("Profile");
    }
}
