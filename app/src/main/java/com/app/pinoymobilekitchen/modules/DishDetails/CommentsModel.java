package com.app.pinoymobilekitchen.modules.DishDetails;


public class CommentsModel {

    private String message;
    private String userName;
    private String userProfPic;

    /**
     * No args constructor for use in serialization
     *
     */
    public CommentsModel() {
    }

    /**
     *
     * @param message
     * @param userProfPic
     * @param userName
     */
    public CommentsModel(String message, String userName, String userProfPic) {
        super();
        this.message = message;
        this.userName = userName;
        this.userProfPic = userProfPic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserProfPic() {
        return userProfPic;
    }

    public void setUserProfPic(String userProfPic) {
        this.userProfPic = userProfPic;
    }

}