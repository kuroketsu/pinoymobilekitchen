package com.app.pinoymobilekitchen.modules.Library;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.pinoymobilekitchen.R;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;

public class UploadActivity extends AppCompatActivity {

    private Button btnSelectVideo, btnSelectThumbnail, btnUpload;
    private TextInputEditText etRecipeName, etDescription, etIngredients, etProcedures;
    private PlayerView vvPreview;
    private SimpleExoPlayer exoPlayer;
    private Spinner spCategory;
    private int REQUEST_CODE_VIDEO = 2;
    private int REQUEST_CODE_IMAGE = 3;
    private String imageFilePath;
    private Uri videoUri, imageUri;
    private Bitmap imgbitmap;
    private ImageView ivThumbnail;
    private TextView tvUploadProgress, tvUploadLabel;
    private ProgressBar pbUpload;
    private LinearLayout llUploadProgress;
    private TextView tvIngredients, tvProcedures;
    private ImageButton ibtnRemoveIngredient, ibtnRemoveProcedure;
    private ArrayList<String> ingredientsList = new ArrayList<>(), procedureList = new ArrayList<>();

    private FirebaseDatabase database;
    private DatabaseReference categoryRef, libraryRef;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private StorageReference mStorageRef;

    private boolean isFullscreen = false;
    private FrameLayout mFullScreenButton;
    private ImageView mFullScreenIcon;
    Dialog mFullScreenDialog;
    private FrameLayout flVideoFrame;
    private long currentPlaybackPosition = 0;
    private ProgressBar pbBuffering;
    private ImageView ivPlay;
    private boolean isRepeated = false;

    @Override
    protected void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = mAuth.getCurrentUser();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnSelectVideo = findViewById(R.id.btnUpload_SelectVideo);
        btnSelectThumbnail = findViewById(R.id.btnUpload_SelectThumbnail);
        btnUpload = findViewById(R.id.btnUpload_Upload);
        etDescription = findViewById(R.id.etUpload_Description);
        etIngredients = findViewById(R.id.etUpload_Ingredients);
        etProcedures = findViewById(R.id.etUpload_Procedures);
        etRecipeName = findViewById(R.id.etUpload_RecipeName);
        vvPreview = findViewById(R.id.vvUpload_VideoPreview);
        spCategory = findViewById(R.id.spUpload_Category);
        ivThumbnail = findViewById(R.id.ivUpload_Thumbnail);
        tvUploadProgress = findViewById(R.id.tvUpload_UploadProgress);
        tvUploadLabel = findViewById(R.id.tvUpload_UploadLabel);
        pbUpload = findViewById(R.id.pbUpload_Progress);
        llUploadProgress = findViewById(R.id.llUpload_UploadProgress);
        flVideoFrame = findViewById(R.id.main_media_frame);
        tvIngredients = findViewById(R.id.tvUpload_Ingredients);
        tvProcedures = findViewById(R.id.tvUpload_Procedures);
        ibtnRemoveIngredient = findViewById(R.id.ibtnUpload_IngredientRemove);
        ibtnRemoveProcedure = findViewById(R.id.ibtnUpload_ProcedureRemove);
        pbBuffering = findViewById(R.id.pbDishDetails_Buffering);
        ivPlay = findViewById(R.id.ivDishDetails_Play);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        database = FirebaseDatabase.getInstance();

        btnSelectVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(UploadActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(UploadActivity.this, new String[]
                            {
                                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                            }, REQUEST_CODE_VIDEO);
                }
                else {
                    selectVideo();
                }
            }
        });

        btnSelectThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(UploadActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(UploadActivity.this, new String[]
                            {
                                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                            }, REQUEST_CODE_IMAGE);
                }
                else {
                    selectImage();
                }
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etDescription.getText().toString().isEmpty() && !etRecipeName.getText().toString().isEmpty() && !getContents(true, false).isEmpty() && !getContents(false, false).isEmpty()) {
                    if(imageUri != null && videoUri != null) {
                        btnUpload.setAlpha(.5f);
                        btnUpload.setEnabled(false);
                        categoryRef = database.getReference("Category").child(spCategory.getSelectedItem().toString()).push();
                        categoryRef.child("Like").setValue("0");
                        categoryRef.child("Dislike").setValue("0");
                        categoryRef.child("Report Count").setValue("0");
                        categoryRef.child("Rate").setValue("0");
                        categoryRef.child("View Count").setValue("0");
                        categoryRef.child("Recipe Name").setValue(etRecipeName.getText().toString());
                        categoryRef.child("Description").setValue(etDescription.getText().toString());
                        categoryRef.child("Ingredients").setValue(getContents(true, false));
                        categoryRef.child("Procedures").setValue(getContents(false, false));
                        categoryRef.child("Uploader ID").setValue(currentUser.getUid());
                        categoryRef.child("Uploader Name").setValue(currentUser.getDisplayName());
                        if(currentUser.getPhotoUrl()!=null) categoryRef.child("Uploader ProfPic").setValue(currentUser.getPhotoUrl().toString());
                        categoryRef.child("Archived").setValue(false);
                        categoryRef.child("Banned").setValue(false);

                        libraryRef = database.getReference("Library").child(currentUser.getUid()).push();
                        libraryRef.child("Video Key").setValue(categoryRef.getKey());
                        libraryRef.child("Category").setValue(spCategory.getSelectedItem().toString());
                        uploadVideo();
                    }
                    else Toast.makeText(UploadActivity.this, "Please Select a Video and Thumbnail to be Uploaded", Toast.LENGTH_LONG).show();
                }
                else Toast.makeText(UploadActivity.this, "Empty Fields Not Allowed", Toast.LENGTH_LONG).show();
            }
        });

        ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(exoPlayer!=null){
                    if(isRepeated){
                        exoPlayer.seekTo(0);
                    }
                    exoPlayer.setPlayWhenReady(true);
                    ivPlay.setVisibility(View.GONE);
                    ivPlay.setImageResource(R.drawable.ic_play);
                }
            }
        });
    }

    public void btnUpload(View v) {
        switch (v.getId()){
            case R.id.ibtnUpload_IngredientAdd:
                if(!etIngredients.getText().toString().isEmpty()) {
                    ingredientsList.add(etIngredients.getText().toString());
                    tvIngredients.setText(getContents(true, true));
                    etIngredients.setText("");
                    if(ingredientsList.size()!=0) ibtnRemoveIngredient.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.ibtnUpload_ProcedureAdd:
                if(!etProcedures.getText().toString().isEmpty()) {
                    procedureList.add(etProcedures.getText().toString());
                    tvProcedures.setText(getContents(false, true));
                    etProcedures.setText("");
                    if(procedureList.size()!=0) ibtnRemoveProcedure.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.ibtnUpload_IngredientRemove:
                if(ingredientsList.size()!=0){
                    ingredientsList.remove(ingredientsList.size()-1);
                    tvIngredients.setText(getContents(true, true));
                    if(ingredientsList.size()==0){
                        ibtnRemoveIngredient.setVisibility(View.GONE);
                        tvIngredients.setText("Ingredients: (Click + to add)");
                    }
                }
                break;
            case R.id.ibtnUpload_ProcedureRemove:
                if(procedureList.size()!=0){
                    procedureList.remove(procedureList.size()-1);
                    tvProcedures.setText(getContents(false, true));
                    if(procedureList.size()==0){
                        ibtnRemoveProcedure.setVisibility(View.GONE);
                        tvProcedures.setText("Procedures: (Click + to add)");
                    }
                }
                break;
        }
    }

    private String getContents(boolean isIngredient, boolean withTitle){
        if(isIngredient) {
            String ingredients = withTitle?"Ingredients:\n\n":"";
            for (int i = 0; i < ingredientsList.size(); i++) {
                if(i!=0) ingredients+="\n\n";
                ingredients += "\u2022 " + ingredientsList.get(i);
            }
            return ingredients;
        }
        else {
            String procedures = withTitle?"Procedures:\n\n":"";
            for (int i = 0; i < procedureList.size(); i++) {
                if(i!=0) procedures+="\n\n";
                procedures += "Step "+ (i+1) + ": " + procedureList.get(i);
            }
            return procedures;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_CODE_VIDEO){
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                selectVideo();
            }
            else{
                Toast.makeText(UploadActivity.this, "You don't have permission to access file location!", Toast.LENGTH_LONG).show();
            }
            return;
        }
        if(requestCode == REQUEST_CODE_IMAGE){
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                selectImage();
            }
            else{
                Toast.makeText(UploadActivity.this, "You don't have permission to access file location!", Toast.LENGTH_LONG).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void selectVideo(){
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQUEST_CODE_VIDEO);
    }

    private void selectImage(){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == REQUEST_CODE_VIDEO && resultCode == RESULT_OK && data != null){
            String mimeType = UploadActivity.this.getContentResolver().getType(data.getData());
            if(mimeType.equals("video/mp4")) {
                videoUri = data.getData();

                flVideoFrame.setVisibility(View.VISIBLE);
                currentPlaybackPosition = 0;
                setUpVideoView();
                /*try {
                    String[] filePathColumn = {MediaStore.Video.Media.DATA};
                    Cursor cursor = UploadActivity.this.getContentResolver().query(videoUri, filePathColumn, null, null, null);
                    if (cursor.moveToFirst()) {
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        videoFilePath = cursor.getString(columnIndex);
                        Toast.makeText(UploadActivity.this, ""+videoFilePath, Toast.LENGTH_SHORT).show();
                    }
                    cursor.close();


                } catch (Exception ex) {
                    ex.printStackTrace();
                }*/
            }
            else {
                showDialog();
            }
        }
        if(requestCode == REQUEST_CODE_IMAGE && resultCode == RESULT_OK && data != null){
            imageUri = data.getData();
            try{
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = UploadActivity.this.getContentResolver().query(imageUri, filePathColumn, null, null, null);
                if (cursor.moveToFirst()) {
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageFilePath = cursor.getString(columnIndex);
                    imgbitmap = BitmapFactory.decodeFile(imageFilePath);
                }
                cursor.close();
                ivThumbnail.setVisibility(View.VISIBLE);
                ivThumbnail.setImageBitmap(imgbitmap);
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(UploadActivity.this);
        alertDialog.setTitle("Incompatible Video");
        alertDialog.setMessage("The video you selected is not in .mp4 format, please try again with compatible video format.");
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void uploadVideo(){
        llUploadProgress.setVisibility(View.VISIBLE);
        tvUploadLabel.setText("Uploading Video...");

        final StorageReference videoStorageRef = mStorageRef.child("food videos").child(System.currentTimeMillis()+".mp4");

        videoStorageRef.putFile(videoUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        videoStorageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                categoryRef.child("Video URL").setValue(uri.toString());
                                uploadThumbnail();
                            }
                        });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        pbUpload.setProgress((int)progress);
                        tvUploadProgress.setText(((int)progress)+"%");
                    }
                });
    }

    private void uploadThumbnail(){
        tvUploadLabel.setText("Uploading Thumbnail...");

        String imageExtension = imageFilePath.substring(imageFilePath.lastIndexOf("."));
        final StorageReference imageStorageRef = mStorageRef.child("food pictures").child(System.currentTimeMillis()+imageExtension);

        imageStorageRef.putFile(imageUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        imageStorageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                categoryRef.child("Video Thumbnail").setValue(uri.toString());
                                showUploadDone();
                            }
                        });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        pbUpload.setProgress((int)progress);
                        tvUploadProgress.setText(((int)progress)+"%");
                    }
                });
    }

    private void showUploadDone() {
        AlertDialog.Builder alerDialog = new AlertDialog.Builder(UploadActivity.this);
        alerDialog.setTitle("Upload Done!");
        alerDialog.setMessage("You will be redirected to your library.");
        alerDialog.setCancelable(true);

        alerDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setResult(RESULT_OK);
                finish();
            }
        });

        alerDialog.show();
    }

    private void setUpVideoView() {
        TrackSelector trackSelector = new DefaultTrackSelector();
        exoPlayer = ExoPlayerFactory.newSimpleInstance(UploadActivity.this, trackSelector);
        vvPreview.setPlayer(exoPlayer);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(UploadActivity.this, Util.getUserAgent(UploadActivity.this, "VideoPlayer"));
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(videoUri);
        exoPlayer.prepare(videoSource);
        exoPlayer.seekTo(currentPlaybackPosition);
        exoPlayer.setPlayWhenReady(false);
        vvPreview.hideController();
        exoPlayer.addListener(new Player.DefaultEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {
                    case Player.STATE_BUFFERING:
                        if (pbBuffering.getVisibility() == View.GONE) {
                            pbBuffering.setVisibility(View.VISIBLE);
                        }
                        //Toast.makeText(DishDetailsActivity.this, "Buffering "+ (pbBuffering.getVisibility()==View.VISIBLE?"Visible":"Gone"), Toast.LENGTH_SHORT).show();
                        break;
                    case Player.STATE_ENDED:
                        vvPreview.hideController();
                        isRepeated = true;
                        ivPlay.setImageResource(R.drawable.ic_replay);
                        ivPlay.setVisibility(View.VISIBLE);
                        break;
                    case Player.STATE_IDLE:
                        ivPlay.setVisibility(View.VISIBLE);
                        vvPreview.hideController();
                        break;
                    case Player.STATE_READY:
                        pbBuffering.setVisibility(View.GONE);
                        vvPreview.hideController();
                        //Toast.makeText(DishDetailsActivity.this, "Ready "+ (pbBuffering.getVisibility()==View.VISIBLE?"Visible":"Gone"), Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
            }
        });

        if(mFullScreenDialog==null) initFullscreenDialog();
        initFullscreenButton();
    }

    private void initFullscreenDialog() {
        Log.e("Dish Details", "closeFullscreenDialog: " + "init dialog");
        mFullScreenDialog = new Dialog(UploadActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (isFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }

    private void openFullscreenDialog() {
        Log.e("Dish Details", "closeFullscreenDialog: " + "open dialog");
        ((ViewGroup) vvPreview.getParent()).removeView(vvPreview);
        ((ViewGroup) pbBuffering.getParent()).removeView(pbBuffering);
        ((ViewGroup) ivPlay.getParent()).removeView(ivPlay);
        mFullScreenDialog.addContentView(vvPreview, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mFullScreenDialog.addContentView(pbBuffering, pbBuffering.getLayoutParams());
        mFullScreenDialog.addContentView(ivPlay, ivPlay.getLayoutParams());
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(UploadActivity.this, R.drawable.ic_fullscreen_skrink));
        isFullscreen = true;
        mFullScreenDialog.show();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    private void closeFullscreenDialog() {
        Log.e("Dish Details", "closeFullscreenDialog: " + "close dialog");
        ((ViewGroup) vvPreview.getParent()).removeView(vvPreview);
        ((ViewGroup) pbBuffering.getParent()).removeView(pbBuffering);
        ((ViewGroup) ivPlay.getParent()).removeView(ivPlay);
        ((FrameLayout) findViewById(R.id.main_media_frame)).addView(vvPreview);
        ((FrameLayout) findViewById(R.id.main_media_frame)).addView(pbBuffering);
        ((FrameLayout) findViewById(R.id.main_media_frame)).addView(ivPlay);
        isFullscreen = false;
        mFullScreenDialog.dismiss();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(UploadActivity.this, R.drawable.ic_fullscreen_expand));
    }

    private void initFullscreenButton() {
        Log.e("Dish Details", "closeFullscreenDialog: " + "init buttons");
        PlaybackControlView controlView = vvPreview.findViewById(R.id.exo_controller);
        controlView.setRewindIncrementMs(5000);
        controlView.setFastForwardIncrementMs(5000);
        mFullScreenIcon = controlView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenButton = controlView.findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFullscreen)
                    openFullscreenDialog();
                else
                    closeFullscreenDialog();
            }
        });
    }

    @Override
    protected void onResume() {
        if(exoPlayer==null) {
            if(videoUri!=null) {
                setUpVideoView();
            }
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if(exoPlayer!=null) {
            currentPlaybackPosition = exoPlayer.getContentPosition();
            exoPlayer.release();
            exoPlayer=null;
        }
        super.onPause();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
