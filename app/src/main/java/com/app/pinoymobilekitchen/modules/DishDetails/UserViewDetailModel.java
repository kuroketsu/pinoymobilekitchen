package com.app.pinoymobilekitchen.modules.DishDetails;

public class UserViewDetailModel {

    private String rating;
    private Boolean isDisliked;
    private Boolean isLiked;
    private Boolean isReported;

    /**
     * No args constructor for use in serialization
     *
     */
    public UserViewDetailModel() {
    }

    /**
     *
     * @param isLiked
     * @param rating
     * @param isDisliked
     */
    public UserViewDetailModel(String rating, Boolean isDisliked, Boolean isLiked, Boolean isReported) {
        super();
        this.rating = rating;
        this.isDisliked = isDisliked;
        this.isLiked = isLiked;
        this.isReported = isReported;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public Boolean getIsDisliked() {
        return isDisliked;
    }

    public void setIsDisliked(Boolean isDisliked) {
        this.isDisliked = isDisliked;
    }

    public Boolean getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Boolean isLiked) {
        this.isLiked = isLiked;
    }

    public Boolean getReported() {
        return isReported;
    }

    public void setReported(Boolean reported) {
        isReported = reported;
    }

}