package com.app.pinoymobilekitchen;

public class DishModel {

    private String category;
    private String key;
    private String dislike;
    private String like;
    private String rate;
    private String description;
    private String ingredients;
    private String procedures;
    private String recipeName;
    private String reportCount;
    private String uploaderID;
    private String uploaderName;
    private String uploaderProfPic;
    private String videoThumbnail;
    private String videoURL;
    private String viewCount;
    private Boolean archived;
    private Boolean banned;
    private String managementKey;

    /**
     * No args constructor for use in serialization
     *
     */
    public DishModel() {
    }

    /**
     *
     * @param uploaderID
     * @param recipeName
     * @param reportCount
     * @param like
     * @param banned
     * @param procedures
     * @param videoThumbnail
     * @param ingredients
     * @param rate
     * @param archived
     * @param description
     * @param uploaderProfPic
     * @param dislike
     * @param videoURL
     * @param viewCount
     * @param key
     * @param uploaderName
     */
    public DishModel(String key, String dislike, String like, String rate, String description, String ingredients, String procedures, String recipeName, String reportCount, String uploaderID, String uploaderName, String uploaderProfPic, String videoThumbnail, String videoURL, String viewCount, Boolean archived, Boolean banned) {
        super();
        this.key = key;
        this.dislike = dislike;
        this.like = like;
        this.rate = rate;
        this.description = description;
        this.ingredients = ingredients;
        this.procedures = procedures;
        this.recipeName = recipeName;
        this.reportCount = reportCount;
        this.uploaderID = uploaderID;
        this.uploaderName = uploaderName;
        this.uploaderProfPic = uploaderProfPic;
        this.videoThumbnail = videoThumbnail;
        this.videoURL = videoURL;
        this.viewCount = viewCount;
        this.archived = archived;
        this.banned = banned;
    }

    public DishModel(String category, String key, String dislike, String like, String rate, String description, String ingredients, String procedures, String recipeName, String reportCount, String uploaderID, String uploaderName, String uploaderProfPic, String videoThumbnail, String videoURL, String viewCount, Boolean archived, Boolean banned) {
        this.category = category;
        this.key = key;
        this.dislike = dislike;
        this.like = like;
        this.rate = rate;
        this.description = description;
        this.ingredients = ingredients;
        this.procedures = procedures;
        this.recipeName = recipeName;
        this.reportCount = reportCount;
        this.uploaderID = uploaderID;
        this.uploaderName = uploaderName;
        this.uploaderProfPic = uploaderProfPic;
        this.videoThumbnail = videoThumbnail;
        this.videoURL = videoURL;
        this.viewCount = viewCount;
        this.archived = archived;
        this.banned = banned;
    }

    public DishModel(String category, String key, String dislike, String like, String rate, String description, String ingredients, String procedures, String recipeName, String reportCount, String uploaderID, String uploaderName, String uploaderProfPic, String videoThumbnail, String videoURL, String viewCount, Boolean archived, Boolean banned, String managementKey) {
        this.category = category;
        this.key = key;
        this.dislike = dislike;
        this.like = like;
        this.rate = rate;
        this.description = description;
        this.ingredients = ingredients;
        this.procedures = procedures;
        this.recipeName = recipeName;
        this.reportCount = reportCount;
        this.uploaderID = uploaderID;
        this.uploaderName = uploaderName;
        this.uploaderProfPic = uploaderProfPic;
        this.videoThumbnail = videoThumbnail;
        this.videoURL = videoURL;
        this.viewCount = viewCount;
        this.archived = archived;
        this.banned = banned;
        this.managementKey = managementKey;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDislike() {
        return dislike;
    }

    public void setDislike(String dislike) {
        this.dislike = dislike;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getProcedures() {
        return procedures;
    }

    public void setProcedures(String procedures) {
        this.procedures = procedures;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getReportCount() {
        return reportCount;
    }

    public void setReportCount(String reportCount) {
        this.reportCount = reportCount;
    }

    public String getUploaderID() {
        return uploaderID;
    }

    public void setUploaderID(String uploaderID) {
        this.uploaderID = uploaderID;
    }

    public String getUploaderName() {
        return uploaderName;
    }

    public void setUploaderName(String uploaderName) {
        this.uploaderName = uploaderName;
    }

    public String getUploaderProfPic() {
        return uploaderProfPic;
    }

    public void setUploaderProfPic(String uploaderProfPic) {
        this.uploaderProfPic = uploaderProfPic;
    }

    public String getVideoThumbnail() {
        return videoThumbnail;
    }

    public void setVideoThumbnail(String videoThumbnail) {
        this.videoThumbnail = videoThumbnail;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public String getViewCount() {
        return viewCount;
    }

    public void setViewCount(String viewCount) {
        this.viewCount = viewCount;
    }

    public Boolean getArchived() {
        return archived;
    }

    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    public Boolean getBanned() {
        return banned;
    }

    public void setBanned(Boolean banned) {
        this.banned = banned;
    }

    public String getManagementKey() {
        return managementKey;
    }

    public void setManagementKey(String managementKey) {
        this.managementKey = managementKey;
    }

}